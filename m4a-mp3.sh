#!/bin/bash
# http://conjurecode.com/converting-m4a-to-mp3-with-ffmpeg-on-linux/

for i in *.m4a
do
    ffmpeg -i "$i" -ab 256k "${i%m4a}mp3" && rm "$i"
done
